array-query-ts
===================

[![Build Status](https://gitlab.com/luisvargastijerino/array-query-ts/badges/main/pipeline.svg)](https://gitlab.com/luisvargastijerino/array-query-ts/-/commits/main)

A lightweight query API plugin for Typescript and ES6 - works in the Browser and on the Server.

### Features

- Search for objects with a Query API similar to [MongoDB](http://www.mongodb.org/display/DOCS/Advanced+Queries)
- Use a complex query object, or build queries up with a chainable api
- Full support for compound queries ($not, $nor, $or, $and), including nested compound queries.
- Full support for querying nested arrays (see `$elemMatch`)
- Accepts dot notation to query deep properties (e.g. {"stats.views.december": 100}

Please report any bugs, feature requests in the issue tracker.
Pull requests are welcome!


Installation
============

You can install with your preferred package manager:
```
npm install array-query-ts

yarn add array-query-ts

pnpm i array-query-ts
```

Then import the methods inside any `ES6/TS` file:

```typescript
import {where} from 'array-query-ts'
```

Basic Usage
===========

The following are some basic examples:

```ts
 myCollection.filter(where({
    featured:true,
    likes: {$gt:10}
}));
// Returns all models where the featured attribute is true and there are
// more than 10 likes

myCollection.filter(where({tags: { $any: ["coffeescript", "backbone", "mvc"]}}));
// Finds models that have either "coffeescript", "backbone", "mvc" in their "tags" attribute

myCollection.filter(where({
  // Models must match all these queries
  $and:{
    title: {$like: "news"}, // Title attribute contains the string "news"
    likes: {$gt: 10}
  }, // Likes attribute is greater than 10

  // Models must match one of these queries
  $or:{
    featured: true, // Featured attribute is true
    category:{$in:["code","programming","javascript"]}
  }
  //Category attribute is either "code", "programming", or "javascript"
}));

titles = query(myCollection)
  .and("published", true)
  .or("likes", {$gt:10})
  .or("tags", ["javascript", "coffeescript"])
  .all()
// Builds a query up programatically
// and Runs the query


query = query()
  .and("published", true)
  .or("likes", {$gt:10})
  .or("tags", ["javascript", "coffeescript"])

resultsA = query.all(collectionA)
resultsB = query.all(collectionB)
// Builds a query and then runs it on 2 seperate collections

```

Query API
===

### $equal
Performs a strict equality test using `===`. If no operator is provided and the query value isn't a regex then `$equal` is assumed.

If the attribute in the model is an array then the query value is searched for in the array in the same way as `$contains`

If the query value is an object (including array) then a deep comparison is performed

```javascript
 myCollection.filter(where({ title:"Test" }));
// Returns all models which have a "title" attribute of "Test"

 myCollection.filter(where({ title: {$equal:"Test"} })); // Same as above

 myCollection.filter(where({ colors: "red" }));
// Returns models which contain the value "red" in a "colors" attribute that is an array.
```

### $contains
Assumes that the model property is an array and searches for the query value in the array

```js
 myCollection.filter(where({ colors: {$contains: "red"} }));
// Returns models which contain the value "red" in a "colors" attribute that is an array.
// e.g. a model with this attribute colors:["red","yellow","blue"] would be returned
```

### $ne
"Not equal", the opposite of $equal, returns all models which don't have the query value

```js
 myCollection.filter(where({ title: {$ne:"Test"} }));
// Returns all models which don't have a "title" attribute of "Test"
```

### $lt, $lte, $gt, $gte
These conditional operators can be used for greater than and less than comparisons in queries

```js
 myCollection.filter(where({ likes: {$lt:10} }));
// Returns all models which have a "likes" attribute of less than 10
 myCollection.filter(where({ likes: {$lte:10} }));
// Returns all models which have a "likes" attribute of less than or equal to 10
 myCollection.filter(where({ likes: {$gt:10} }));
// Returns all models which have a "likes" attribute of greater than 10
 myCollection.filter(where({ likes: {$gte:10} }));
// Returns all models which have a "likes" attribute of greater than or equal to 10
```

These may further be combined:

```js
 myCollection.filter(where({ likes: {$gt:2, $lt:20} }));
// Returns all models which have a "likes" attribute of greater than 2 or less than 20
// This example is also equivalent to $between: [2,20]
 myCollection.filter(where({ likes: {$gte:2, $lte:20} }));
// Returns all models which have a "likes" attribute of greater than or equal to 2, and less than or equal to 20
 myCollection.filter(where({ likes: {$gte:2, $lte: 20, $ne: 12} }));
// Returns all models which have a "likes" attribute between 2 and 20 inclusive, but not equal to 12
```



### $between
To check if a value is in-between 2 query values use the $between operator and supply an array with the min and max value

```js
 myCollection.filter(where({ likes: {$between:[5, 15] } }));
// Returns all models which have a "likes" attribute of greater than 5 and less then 15
```

### $in
An array of possible values can be supplied using $in, a model will be returned if any of the supplied values is matched

```js
 myCollection.filter(where({ title: {$in:["About", "Home", "Contact"] } }));
// Returns all models which have a title attribute of either "About", "Home", or "Contact"
```

### $nin
"Not in", the opposite of $in. A model will be returned if none of the supplied values is matched

```js
 myCollection.filter(where({ title: {$nin:["About", "Home", "Contact"] } }));
// Returns all models which don't have a title attribute of either
// "About", "Home", or "Contact"
```

### $all
Assumes the model property is an array and only returns models where all supplied values are matched.

```js
 myCollection.filter(where({ colors: {$all:["red", "yellow"] } }));
// Returns all models which have "red" and "yellow" in their colors attribute.
// A model with the attribute colors:["red","yellow","blue"] would be returned
// But a model with the attribute colors:["red","blue"] would not be returned
```

### $any
Assumes the model property is an array and returns models where any of the supplied values are matched.

```js
 myCollection.filter(where({ colors: {$any:["red", "yellow"] } }));
// Returns models which have either "red" or "yellow" in their colors attribute.
```

### $size
Assumes the model property has a length (i.e. is either an array or a string).
Only returns models the model property's length matches the supplied values

```js
 myCollection.filter(where({ colors: {$size:2 } }));
// Returns all models which 2 values in the colors attribute
```

### $exists or $has
Checks for the existence of an attribute. Can be supplied either true or false.

```js
 myCollection.filter(where({ title: {$exists: true } }));
// Returns all models which have a "title" attribute
 myCollection.filter(where({ title: {$has: false } }));
// Returns all models which don't have a "title" attribute
```

### $like
Assumes the model attribute is a string and checks if the supplied query value is a substring of the property.
Uses indexOf rather than regex for performance reasons

```js
 myCollection.filter(where({ title: {$like: "Test" } }));
//Returns all models which have a "title" attribute that
//contains the string "Test", e.g. "Testing", "Tests", "Test", etc.
```

### $likeI
The same as above but performs a case-insensitive search using indexOf and toLowerCase (still faster than Regex)

```js
 myCollection.filter(where({ title: {$likeI: "Test" } }));
//Returns all models which have a "title" attribute that
//contains the string "Test", "test", "tEst","tesT", etc.
```

### $regex
Checks if the model attribute matches the supplied regular expression. The regex query can be supplied without the `$regex` keyword

```js
 myCollection.filter(where({ content: {$regex: /coffeescript/gi } }));
// Checks for a regex match in the content attribute
 myCollection.filter(where({ content: /coffeescript/gi }));
// Same as above
```

### $cb
A callback function can be supplied as a test. The callback will receive the attribute and should return either true or false.
`this` will be set to the current model, this can help with tests against computed properties

```ts
myCollection.filter(where({ title: {$cb: attr => attr.charAt(0) === "c"}} ));
// Returns all models that have a title attribute that starts with "c"

_.query( myCollection, { computed_test: {$cb: function(){ return this.computed_property() > 10;}} });
// Returns all models where the computed_property method returns a value greater than 10.
```

For callbacks that use `this` rather than the model attribute, the key name supplied is arbitrary and has no
effect on the results. If the only test you were performing was like the above test it would make more sense
to simply use `myCollection.filter`. However, if you are performing other tests or are using the paging / sorting /
caching options of backbone query, then this functionality is useful.

### $elemMatch
This operator allows you to perform queries in nested arrays similar to [MongoDB](http://www.mongodb.org/display/DOCS/Advanced+Queries#AdvancedQueries-%24elemMatch)
For example you may have a collection of models in with this kind of data structure:

```js
var posts = [
    {title: "Home", comments:[
      {text:"I like this post"},
      {text:"I love this post"},
      {text:"I hate this post"}
    ]},
    {title: "About", comments:[
      {text:"I like this page"},
      {text:"I love this page"},
      {text:"I really like this page"}
    ]}
];
```
To search for posts which have the text "really" in any of the comments you could search like this:

```js
post.filter(where({
  comments: {
    $elemMatch: {
      text: /really/i
    }
  }
}));
```

All the operators above can be performed on `$elemMatch` queries, e.g. `$all`, `$size` or `$lt`.
`$elemMatch` queries also accept compound operators, for example this query searches for all posts that
have at least one comment without the word "really" and with the word "totally".
```js
post.filter(where({
  comments: {
    $elemMatch: {
      $not: {
        text: /really/i
      },
      $and: {
        text: /totally/i
      }
    }
  }
}));
```


### $computed
This operator allows you to perform queries on computed properties. For example, you may want to perform a query
for a persons full name, even though the first and last name are stored separately in your db / model.
For example

```ts
class Person {
  firstName: string;
  lastName: string;

  get fullName() {
    return this.firstName + " " + this.lastName;
  }

  constructor(other?: Person) {
    Object.assign(this, other)
  }
};

const a = new Person({
  firstName: "Dave",
  lastName: "Tonge"
});

const b = new Person({
  firstNme: "John",
  lastName: "Smith"
});

const persons = [a, b];

persons.filter(where({
  fullName: {$computed: "Dave Tonge"}
}));
// Returns the model with the computed `fullName` equal to Dave Tonge

persons.filter(where({
  fullName: {$computed: {$likeI: "john smi"}}
}));
// Any of the previous operators can be used (including elemMatch is required)
```


Combined Queries
================

Multiple queries can be combined. By default, all supplied queries use the `$and` operator. However, it is possible
to specify either `$or`, `$nor`, `$not` to implement alternate logic.

### $and

```js
 myCollection.filter(where({ $and: { title: {$like: "News"}, likes: {$gt: 10}}}));
// Returns all models that contain "News" in the title and have more than 10 likes.
 myCollection.filter(where({ title: {$like: "News"}, likes: {$gt: 10} }));
// Same as above as $and is assumed if not supplied
```

### $or

```js
 myCollection.filter(where({ $or: { title: {$like: "News"}, likes: {$gt: 10}}}));
// Returns all models that contain "News" in the title OR have more than 10 likes.
```

### $nor
The opposite of `$or`

```js
 myCollection.filter(where({ $nor: { title: {$like: "News"}, likes: {$gt: 10}}}));
// Returns all models that don't contain "News" in the title NOR have more than 10 likes.
```

### $not
The opposite of `$and`

```js
 myCollection.filter(where({ $not: { title: {$like: "News"}, likes: {$gt: 10}}}));
// Returns all models that don't contain "News" in the title AND DON'T have more than 10 likes.
```

If you need to perform multiple queries on the same key, then you can supply the query as an array:
```js
 myCollection.filter(where({
    $or:[
        {title:"News"},
        {title:"About"}
    ]
}));
// Returns all models with the title "News" or "About".
```


Compound Queries
================

It is possible to use multiple combined queries, for example searching for models that have a specific title attribute,
and either a category of "abc" or a tag of "xyz"

```js
 myCollection.filter(where({
    $and: { title: {$like: "News"}},
    $or: {likes: {$gt: 10}, color: {$contains:"red"}}
}));
//Returns models that have "News" in their title and
//either have more than 10 likes or contain the color red.
```


Chainable API
=============

Rather than supplying a single query object, you can build up the query bit by bit:

```javascript
query(myCollection)
  .and("published", true)
  .or("likes", {$gt: 10})
  .or("tags", ["javascript", "coffeescript"])
  .run()
```

Instead of calling `where`, we call `buildQuery`. This returns a query object that we can build before running.
`buildQuery` can take the collection that you want to query, or alternatively you can pass the collection in when
running the query. Therefore, these 2 both give the same results:

```javascript
results = query(myCollection).and("published", true).run()
results = query().and("published", true).run(myCollection)
```

To build the query you can call `.and`, `.or`, `.nor` and `.not`.
These methods can accept either a query object, or a query key and a query value.  For example the following two examples
are the same.

```javascript
results = query(myCollection).and({"published": true}).run()
results = query(myCollection).and("published", true).run()
```

To run the query you can call either `.run`, `.find`, or `.all`.
These methods are all aliases to each other and will run the query returning an array of results.

To retrieve just the first results you can use `.first`. For example:

```javascript
firstResult = query(myCollection).and({"published": true}).first()
```

[//]: # (Indexing)

[//]: # (========)

[//]: # ()
[//]: # ()
[//]: # (More documentation coming...)

[//]: # (Essentially you can add indexes when using the chainable syntax.)

[//]: # (You can then perform queries as usual, but the results, should be faster on larger sets)

[//]: # (I suggest that you benchmark your code to test this out.)

[//]: # (The index method takes either a single key, or a key and a function.)

[//]: # ()
[//]: # ()
[//]: # (```ts)

[//]: # ()
[//]: # (    query = _.query&#40;array&#41;)

[//]: # (      .index&#40;"title"&#41;;)

[//]: # ()
[//]: # (    // could have been .index&#40;"title", &#40;obj&#41; -> obj.title&#41;)

[//]: # ()
[//]: # (    result = query.and&#40;"title", "Home"&#41;.run&#40;&#41;)

[//]: # (```)


Contributors
===========

Dave Tonge - [davidgtonge](http://github.com/davidgtonge)
Rob W - [Rob W](https://github.com/Rob--W)
Cezary Wojtkowski - [cezary](https://github.com/cezary)
