/*
Arrays Query TS - A lightweight query API for Typescript collections
(c)2022 - Luis Vargas
May be freely distributed according to MIT license.

This is small library that provides a query api for JavaScript arrays similar to *mongo db*.
The aim of the project is to provide a simple, well tested, way of filtering data in JavaScript.
 */

function indexOf(item) {
  for (let i = 0; i < this.length; i++) {
    if (this[i].constructor.name === 'RegExp') {
      if (i in this && this[i].test(item)) return i;
    } else {
      if (i in this && this[i] === item) return i;
    }
  }
  return -1;
}

/* UTILS */
const utils = {
  result: (obj, key) => {
    if (obj == null) {
      obj = {};
    }
    if (utils.getType(obj[key]) === "Function") {
      return obj[key]();
    } else {
      return obj[key];
    }
  },
  /*detect: (array, fn) => {
    let i, item, len;
    for (i = 0, len = array.length; i < len; i++) {
      item = array[i];
      if (fn(item)) {
        return item;
      }
    }
  },
  reject: (array, fn) => {
    let i, item, len;
    const results: unknown[] = [];
    for (i = 0, len = array.length; i < len; i++) {
      item = array[i];
      if (!fn(item)) {
        results.push(item);
      }
    }
    return results;
  },*/
  intersection: (array1, array2) => {
    let i, item, len;
    const results: unknown[] = [];
    for (i = 0, len = array1.length; i < len; i++) {
      item = array1[i];
      if (array2.indexOf(item) !== -1) {
        results.push(item);
      }
    }
    return results;
  },
  isEqual: (a, b) => JSON.stringify(a) === JSON.stringify(b),
  getType(obj) {
    const type = Object.prototype.toString.call(obj).substring(8);
    return type.substring(0, type.length - 1);
  },

  makeObj(key, val) {
    let o;
    (o = {})[key] = val;
    return o;
  },

  reverseString(str) {
    return str.toLowerCase().split('').reverse().join('');
  },

  compoundKeys: ["$and", "$not", "$or", "$nor"],

  makeGetter(keys) {
    keys = keys.split(".");
    return obj => {
      let i, key, len, out;
      out = obj;
      for (i = 0, len = keys.length; i < len; i++) {
        key = keys[i];
        if (out) {
          out = utils.result(out, key);
        }
      }
      return out;
    };
  }
}

function multipleConditions(key, queries) {
  let type, val;
  const results: unknown[] = [];
  for (type in queries) {
    val = queries[type];
    results.push(utils.makeObj(key, utils.makeObj(type, val)));
  }
  return results;
}

interface ParamType {
  key: string | null;
  boost: unknown;
  getter: (key: string) => unknown;
  type: string;
  value: RegExp | Date | object | ParamType[] | ((model) => boolean) | string | boolean | null | number
}

function parseParamType(query) {
  const key = Object.keys(query)[0];
  const queryParam = query[key];
  let o = {
    key
  } as ParamType;
  if (queryParam != null ? queryParam.$boost : void 0) {
    o.boost = queryParam.$boost;
    delete queryParam.$boost;
  }
  if (key.indexOf(".") !== -1) {
    o.getter = utils.makeGetter(key);
  }
  const paramType = utils.getType(queryParam);
  switch (paramType) {
    case "RegExp":
    case "Date":
      o.type = `$${paramType.toLowerCase()}`;
      o.value = queryParam;
      break;
    case "Object":
      if (indexOf.call(utils.compoundKeys, key) >= 0) {
        o.type = key;
        o.value = parseSubQuery(queryParam);
        o.key = null;
      } else if (Object.keys(queryParam).length > 1) {
        o.type = "$and";
        o.value = parseSubQuery(multipleConditions(key, queryParam));
        o.key = null;
      } else {
        for (const type in queryParam) {
          if (!queryParam.hasOwnProperty(type)) continue;
          const value = queryParam[type];
          if (testQueryValue(type, value)) {
            o.type = type;
            switch (type) {
              case "$elemMatch":
                o.value = single(parseQuery(value));
                break;
              case "$endsWith":
                o.value = utils.reverseString(value);
                break;
              case "$likeI":
              case "$startsWith":
                o.value = value.toLowerCase();
                break;
              case "$not":
              case "$nor":
              case "$or":
              case "$and":
                o.value = parseSubQuery(utils.makeObj(o.key, value));
                o.key = null;
                break;
              case "$computed":
                o = parseParamType(utils.makeObj(key, value));
                o.getter = utils.makeGetter(key);
                break;
              default:
                o.value = value;
            }
          } else {
            throw new Error("Query value (" + value + ") doesn't match query type: (" + type + ")");
          }
        }
      }
      break;
    default:
      o.type = "$equal";
      o.value = queryParam;
  }
  if ((o.type === "$equal") && (paramType === "Object" || paramType === "Array")) {
    o.type = "$deepEqual";
  }
  return o;
}

function parseSubQuery(rawQuery) {
  let i, key, len, query, queryArray, val;
  if (Array.isArray(rawQuery)) {
    queryArray = rawQuery;
  } else {
    queryArray = (function () {
      const results: unknown[] = [];
      for (key in rawQuery) {
        if (!rawQuery.hasOwnProperty(key)) continue;
        val = rawQuery[key];
        results.push(utils.makeObj(key, val));
      }
      return results;
    })();
  }
  const results: ParamType[] = [];
  for (i = 0, len = queryArray.length; i < len; i++) {
    query = queryArray[i];
    results.push(parseParamType(query));
  }
  return results;
}

function testQueryValue(queryType, value) {
  const valueType = utils.getType(value);
  switch (queryType) {
    case "$in":
    case "$nin":
    case "$all":
    case "$any":
      return valueType === "Array";
    case "$size":
      return valueType === "Number";
    case "$regex":
    case "$regexp":
      return valueType === "RegExp";
    case "$like":
    case "$likeI":
      return valueType === "String";
    case "$between":
    case "$mod":
      return (valueType === "Array") && (value.length === 2);
    case "$cb":
      return valueType === "Function";
    default:
      return true;
  }
}

function testModelAttribute(queryType, value) {
  const valueType = utils.getType(value);
  switch (queryType) {
    case "$like":
    case "$likeI":
    case "$regex":
    case "$startsWith":
    case "$endsWith":
      return valueType === "String";
    case "$contains":
    case "$all":
    case "$any":
    case "$elemMatch":
      return valueType === "Array";
    case "$size":
      return valueType === "String" || valueType === "Array";
    case "$in":
    case "$nin":
      return value != null;
    default:
      return true;
  }
}

function performQuery(type, value, attr, model, getter) {
  switch (type) {
    case "$equal":
      if (Array.isArray(attr)) {
        return indexOf.call(attr, value) >= 0;
      } else {
        return attr === value;
      }
    case "$deepEqual":
      return utils.isEqual(attr, value);
    case "$contains":
      return indexOf.call(attr, value) >= 0;
    case "$ne":
      return attr !== value;
    case "$lt":
      return attr < value;
    case "$gt":
      return attr > value;
    case "$lte":
      return attr <= value;
    case "$gte":
      return attr >= value;
    case "$between":
      return (value[0] < attr && attr < value[1]);
    case "$betweene":
      return (value[0] <= attr && attr <= value[1]);
    case "$in":
      return indexOf.call(value, attr) >= 0;
    case "$nin":
      return indexOf.call(value, attr) < 0;
    case "$all":
      return value.every(item => indexOf.call(attr, item) >= 0);
    case "$any":
      return attr.some(item => indexOf.call(value, item) >= 0);
    case "$size":
      return attr.length === value;
    case "$exists":
    case "$has":
      return (attr != null) === value;
    case "$like":
      return attr.indexOf(value) !== -1;
    case "$likeI":
      return attr.toLowerCase().indexOf(value) !== -1;
    case "$startsWith":
      return attr.toLowerCase().indexOf(value) === 0;
    case "$endsWith":
      return utils.reverseString(attr).indexOf(value) === 0;
    case "$type":
      return typeof attr === value;
    case "$regex":
    case "$regexp":
      return value.test(attr);
    case "$cb":
      return value.call(model, attr);
    case "$mod":
      return (attr % value[0]) === value[1];
    case "$elemMatch":
      return attr.find(where(value));
    case "$and":
    case "$or":
    case "$nor":
    case "$not":
      return performQuerySingle(type, value, getter, model);
    default:
      return false;
  }
}

function single(queries, getter?, isScore = false) {
  let method, queryObj;
  if (utils.getType(getter) === "String") {
    method = getter;
    getter = function (obj, key) {
      return obj[method](key);
    };
  }
  if (isScore) {
    if (queries.length !== 1) {
      throw new Error("score operations currently don't work on compound queries");
    }
    queryObj = queries[0];
    if (queryObj.type !== "$and") {
      throw new Error("score operations only work on $and queries (not " + queryObj.type);
    }
    return function (model) {
      model._score = performQuerySingle(queryObj.type, queryObj.parsedQuery, getter, model, true);
      return model;
    };
  } else {
    return function (model) {
      for (const item of queries) {
        queryObj = item;
        if (!performQuerySingle(queryObj.type, queryObj.parsedQuery, getter, model, isScore)) {
          return false;
        }
      }
      return true;
    };
  }
}

function performQuerySingle(type, query, getter, model, isScore = false) {
  let attr, boost, q, ref, test, passes = 0, score = 0;
  const scoreInc = 1 / query.length;
  for (let i = 0; i < query.length; i++) {
    q = query[i];
    if (q.getter) {
      attr = q.getter(model, q.key);
    } else if (getter) {
      attr = getter(model, q.key);
    } else {
      attr = model[q.key];
    }
    test = testModelAttribute(q.type, attr);
    if (test) {
      test = performQuery(q.type, q.value, attr, model, getter);
    }
    if (test) {
      passes++;
      if (isScore) {
        boost = (ref = q.boost) != null ? ref : 1;
        score += scoreInc * boost;
      }
    }
    switch (type) {
      case "$and":
        if (!isScore) {
          if (!test) {
            return false;
          }
        }
        break;
      case "$not":
        if (test) {
          return false;
        }
        break;
      case "$or":
        if (test) {
          return true;
        }
        break;
      case "$nor":
        if (test) {
          return false;
        }
        break;
      default:
        throw new Error("Invalid compound method");
    }
  }
  if (isScore) {
    return score;
  } else if (type === "$not") {
    return passes === 0;
  } else {
    return type !== "$or";
  }
}

interface ParsedQuery {
  type: string;
  parsedQuery: ParamType[]
}

function parseQuery(query): ParsedQuery[] {
  let key, type, val;
  const queryKeys = Object.keys(query);
  if (!queryKeys.length) {
    return [];
  }
  const compoundQuery = utils.intersection(utils.compoundKeys, queryKeys);
  if (compoundQuery.length === 0) {
    return [{
      type: "$and",
      parsedQuery: parseSubQuery(query)
    }];
  } else {
    if (compoundQuery.length !== queryKeys.length) {
      if (indexOf.call(compoundQuery, "$and") < 0) {
        query.$and = {};
        compoundQuery.unshift("$and");
      }
      for (key in query) {
        if (!query.hasOwnProperty(key)) continue;
        val = query[key];
        if (!(indexOf.call(utils.compoundKeys, key) < 0)) {
          continue;
        }
        query.$and[key] = val;
        delete query[key];
      }
    }
    let i, len;
    const results: ParsedQuery[] = [];
    for (i = 0, len = compoundQuery.length; i < len; i++) {
      type = compoundQuery[i];
      results.push({
        type: type,
        parsedQuery: parseSubQuery(query[type])
      });
    }
    return results;
  }
}

type Getter<T> = string | keyof T | ((obj: T, key: keyof T) => T[keyof T]);

// function parseGetter<T>(getter: Getter<T>) {
//   if (typeof getter === 'string') {
//     getter = (obj, key) => obj[getter as string](key);
//   }
//   return getter;
// }

class QueryBuilder<T> {
  private readonly theQuery: Query<T>;
  private indexes: unknown;

  constructor(
    private items?: T[],
    private _getter?: Getter<T>) {
    this.theQuery = {} as Query<T>;
  }

  all(items?: T[]) {
    if (items) {
      this.items = items;
    }
    if (this.indexes) {
      items = this.getIndexedItems(this.items);
    } else {
      items = this.items ?? [];
    }
    return items.filter(where(this.theQuery, this._getter)) as (T | Scored<T>)[];
  }

  /*tester() {
    return makeTest(this.theQuery, this._getter);
  }*/

  first(items) {
    return items.find(where(this.theQuery));
  }

  getter(_getter) {
    this._getter = _getter;
    return this;
  }

  run = this.all

  private getIndexedItems(items?: T[]) {
    return items ?? []
  }

  private addToQuery(type: string) {
    return (params, qVal?) => {
      let base;
      if (qVal) {
        params = utils.makeObj(params, qVal);
      }
      if ((base = this.theQuery)[type] == null) {
        base[type] = [];
      }
      this.theQuery[type].push(params);
      return this;
    };
  }

  and = this.addToQuery('$and')
  not = this.addToQuery('$not')
  or = this.addToQuery('$or')
  nor = this.addToQuery('$nor')
}

export function query<T>(items?: T[], getter?: Getter<T>) {
  return new QueryBuilder(items, getter);
}

/*function makeTest(query, getter) {
  return single(parseQuery(query), parseGetter(getter));
}*/

type Scored<T> = T & { _score: number };

type PrimitiveMatcher<T> = T extends string ? string
  : T extends number ? number
  : T extends Date ? Date
  : T extends RegExp ? RegExp
  : undefined;

type QMatcher<T> = {
  $cb?: (attr?: T) => boolean,
  $gt?: number,
  $gte?: number,
  $lt?: number,
  $lte?: number,
  $between?: [number, number],
  $betweene?: [number, number],
  $mod?: [number, number],
  $ne?: T
  $equal?: T | unknown,
  $contains?: T | unknown,
  $in?: T | unknown,
  $nin?: T | unknown,
  $all?: T | unknown,
  $any?: T | unknown,
  $size?: T | unknown,
  $exists?: boolean,
  $like?: string,
  $likeI?: string,
  $startsWith?: string,
  $endsWith?: string,
  $regex?: RegExp,
  $not?: QMatcher<T> | PrimitiveMatcher<T>,
  $elemMatch?: Query<T>,
};

type Query<T> = {
  [P in keyof T]: T[P] | QMatcher<T[P]>;
} | {
  $and?: Query<T> | Query<T>[];
  $or?: Query<T> | Query<T>[];
  $not?: Query<T>;
  $nor?: Query<T> | Query<T>[];
} | Record<string | keyof T, string | number | RegExp | Date | QMatcher<unknown>> /*| ((T) => boolean)*/;

export function where<I, T extends Partial<I>>(query: Query<T>, getter?: Getter<T>): ((item: I) => boolean) {
  // if (getter) {
  //   getter = parseGetter(getter);
  // }
  if (typeof query !== "function") {
    const queries = parseQuery(query);
    return model => {
      for (const queryObj of queries) {
        if (!performQuerySingle(queryObj.type, queryObj.parsedQuery, getter, model, false)) {
          return false;
        }
      }
      return true;
    };
  }
  return query
}

export function score<T>(items: T[], query) {
  // if (getter) {
  //   getter = parseGetter(getter);
  // }
  if (typeof query !== "function") {
    query = single(parseQuery(query), null, true);
  }
  return items.map(query) as Scored<T>[]
}
